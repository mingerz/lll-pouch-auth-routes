const request = require("request");
const PouchDB = require("pouchdb");
const crypto = require("crypto");
const CryptoJS = require("crypto-js");

const authenticateToken = token => {
  return {
    isAuth: true,
    err: undefined
  };
};

const generateCouchProxyToken = username => {
  console.log("Username:", username);
  const secret = "lifelinelabs";
  let token = CryptoJS.HmacSHA1(username, secret).toString(CryptoJS.enc.Hex);
  return token;
};

module.exports = {
  authenticateToken,
  generateCouchProxyToken
};
