const http = require("http");
const Koa = require("koa");
const httpProxy = require("http-proxy");
const helmet = require("koa-helmet");
var cors = require("koa2-cors");

const { userAgent } = require("koa-useragent");
const { authenticateToken, generateCouchProxyToken } = require("./api/main.js");
const app = new Koa();

app.use(cors({ credentials: true }));
app.use(helmet());
app.use(userAgent);

var proxy = httpProxy.createProxyServer();

proxy.on("proxyRes", (proxyRes, req, res) => {
  res.on("data", chunk => {
    data.push(chunk);
  });
  res.on("end", () => {
    if (data.length > 0) {
      console.log(JSON.parse(data));
      console.log("---------- PROXY RES ----------------");
    }
  });
});

const customBodyParser = req => {
  const data = [];
  req.on("data", chunk => {
    data.push(chunk);
  });
  req.on("end", () => {
    if (data.length > 0) {
      try {
        const bodyData = JSON.parse(data);
        console.log(bodyData);
        console.log("---------- PROXY REQ ----------------");
      } catch (e) {
        console.log("ERROR: ", e);
      }
    }
  });
  return data;
};

app.use(async (ctx, next) => {
  await next();
});

app.use(async (ctx, next) => {
  const jwt = ctx.header.Authorization;
  const { isAuth, err } = authenticateToken(jwt);
  if (isAuth === true) {
    const username = ctx.get("x-auth-couchdb-username");
    const couchDbProxyToken = generateCouchProxyToken(username);
    ctx.req.headers["X-Auth-CouchDB-Token"] = couchDbProxyToken;
    await next();
  } else {
    throw new Error("token not authenticated", err);
  }
});

app.use(async (ctx, next) => {
  customBodyParser(ctx.req);
  ctx.respond = false;
  proxy.web(
    ctx.req,
    ctx.res,
    { target: `http://couchdb-0.docker.com:5984/${ctx.header.db}` },
    e => {
      console.log("ERROR");
      console.log(e);
    }
  );
  await next();
});

app.use(async (ctx, next) => {
  console.log("Running from second", "====");
});

// http.createServer(app.callback()).listen(5050);
app.listen(5050);
