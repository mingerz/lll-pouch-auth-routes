echo "install node 1 ..."
docker run -itd -p 35984:5984 -p 35986:5986 --name=couchdb0 \
-e NODENAME='couchdb-0.local.com' \
--mount 'source=volume-0,target=/opt/couchdb/data' \
couchdb:2.3.0

echo "install node 2 ..."
docker run -itd -p 45984:5984 -p 45986:5986 --name=couchdb1 \
-e NODENAME='couchdb-1.local.com' \
--mount 'source=volume-1,target=/opt/couchdb/data' \
couchdb:2.3.0

echo "install node 3 ..."
docker run -itd -p 55984:5984 -p 55986:5986 --name=couchdb2 \
-e NODENAME='couchdb-2.local.com' \
--mount 'source=volume-2,target=/opt/couchdb/data' \
couchdb:2.3.0

echo "waiting for installation of nodes..."
sleep 5

echo "check healthy..."
curl -X GET "http://localhost:35984"
curl -X GET "http://localhost:45984"
curl -X GET "http://localhost:55984"

echo "set admins..."
curl -X PUT "http://localhost:35984/_node/couchdb@couchdb-0.local.com/_config/admins/admin" -d '"1234567"'
curl -X PUT "http://localhost:45984/_node/couchdb@couchdb-1.local.com/_config/admins/admin" -d '"1234567"'
curl -X PUT "http://localhost:55984/_node/couchdb@couchdb-2.local.com/_config/admins/admin" -d '"1234567"'

echo "create network..."
docker network create -d bridge --subnet 172.25.0.0/16 couchdb_nw

docker network connect --alias couchdb-0.local.com \
couchdb_nw couchdb0

docker network connect --alias couchdb-1.local.com \
couchdb_nw couchdb1

docker network connect --alias couchdb-2.local.com \
couchdb_nw couchdb2

docker network inspect couchdb_nw

echo "all done!"

echo "http://localhost:35984/_utils"
echo "http://localhost:45984/_utils"
echo "http://localhost:55984/_utils"
