const request = require("request");
const PouchDB = require("pouchdb");
const crypto = require("crypto");
const CryptoJS = require("crypto-js");

const user = {
  _id: "org.couchdb.user:admin",
  _rev: "1-2560ab3b81cf9a3072621d311ff746b5",
  name: "admin",
  type: "user",
  roles: ["admin", "superadmin", "dev1"],
  password_scheme: "pbkdf2",
  iterations: 10,
  derived_key: "620f6b8d12e9fd98721fcbb43e8cf55929021bf0",
  salt: "6af33b45095f15e4e540b9fcb52517c2"
};

const username = user.name;
const userroles = user.roles
// const secret = "0123456789abcdef0123456789abcdef"; // match secret to docker-compose.yml
const secret = "lifelinelabs"
let token = CryptoJS.HmacSHA1(username, secret).toString(CryptoJS.enc.Hex);

console.log("token: ", token);

var db = new PouchDB("http://localhost:5984/mydb", {
  fetch: function(url, opts) {
    opts.headers.set("X-Auth-CouchDB-Roles", userroles);
    opts.headers.set("X-Auth-CouchDB-UserName", username);
    opts.headers.set("X-Auth-CouchDB-Token", token);
    return PouchDB.fetch(url, opts);
  }
});

db.get("2020-01-10T02:42:32.606Z")
  .then(function(doc) {
    console.log("docs:", doc);
  })
  .catch(function(err) {
    console.log("docs read error:", err);
  });
