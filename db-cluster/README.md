### Quick Start

1. Build the docker image `docker build -t lll/couch-proxy-auth:v1`
2. Start the cluster `docker-compose up`
3. Login to dashboard (username: admin | password: 1234567)
4. Run the test file `node testProxyAuth.js` to create a DB
5. Create a role for mydb called dev
6. Run the testProxyAuth.js again to check that proxy auth works

=================================================================

First of all, setup 3 couchdb nodes using docker:

```bash
echo "install node 1 ..."
docker run -itd -p 35984:5984 -p 35986:5986 --name=couchdb0 \
-e NODENAME='couchdb-0.local.com' \
--mount 'source=volume-0,target=/opt/couchdb/data' \
couchdb:2.3.0

echo "install node 2 ..."
docker run -itd -p 45984:5984 -p 45986:5986 --name=couchdb1 \
-e NODENAME='couchdb-1.local.com' \
--mount 'source=volume-1,target=/opt/couchdb/data' \
couchdb:2.3.0

echo "install node 3 ..."
docker run -itd -p 55984:5984 -p 55986:5986 --name=couchdb2 \
-e NODENAME='couchdb-2.local.com' \
--mount 'source=volume-2,target=/opt/couchdb/data' \
couchdb:2.3.0
```

Open bash for first couchdb node (couchdb0):

```bash
docker exec -it couchdb0 /bin/bash
```

now, go to couchdb config folder path

```bash
cd opt/couchdb/etc/
```

and edit local.ini. add to following sections for enable proxy auth.

```bash
[chttpd]
  authentication_handlers = {chttpd_auth, cookie_authentication_handler}, {chttpd_auth, proxy_authentication_handler}, {chttpd_auth, default_authentication_handler}
[couch_httpd_auth]
  proxy_use_secret = true
```

exactly same edit in rest of nodes (couchdb1, couchdb2).

in first node, go to /opt/couchdb/etc/local.d and open docker.ini.

```bash
[couch_httpd_auth]
  secret = 4df77ee16b8fc1b6577197ac107efeac
```

copy secret code from there.

we need restart the nodes to apply changes:

```bash
docker container stop couchdb0
docker container stop couchdb1
docker container stop couchdb2
docker container start couchdb0
docker container start couchdb1
docker container start couchdb2
```

setup a network for these nodes:

```bash
docker network create -d bridge --subnet 172.25.0.0/16 couchdb_nw

docker network connect --alias couchdb-0.local.com \
couchdb_nw couchdb0

docker network connect --alias couchdb-1.local.com \
couchdb_nw couchdb1

docker network connect --alias couchdb-2.local.com \
couchdb_nw couchdb2
```

check network status:

```bash
docker network inspect couchdb_nw
```

open first node using utils:

```bash
http://localhost:35984/_utils
```

configure a cluster and these nodes to cluster:

```bash
couchdb-1.local.com
couchdb-2.local.com
```

all done. add user to \_users database by adding document following this structure:

```bash
{
  "_id": "org.couchdb.user:username",
  "name": "username",
  "type": "user",
  "roles": [
    "admin",
    "superadmin"
  ],
  "password": "pass",
}
```

in couchdb, create a database and go to configs and add admins and members and roles!
add some test documents to new database.

done! start testing using defined user and roles.
