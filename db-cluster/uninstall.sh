docker stop $(docker ps -aq)
docker system prune
docker volume prune
docker rm couchdb0
docker rm couchdb1
docker rm couchdb2

docker network rm couchdb_nw