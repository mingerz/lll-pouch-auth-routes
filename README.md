### Flow

Frontend Web client -> Caddy Reverse Proxy -> Middleware -> CouchDB

### How to start

1. build docker image lll/couch-proxy-auth:v1

```
cd db-cluster
docker build -t lll/couch-proxy-auth:v1 .
```

2. `docker-compose up` to start backend services
3. `cd web`
4. `http-server .` to spin up a http-server with index.html
5. go to _localhost:5984/\_utils_ and create a DB called Db1 \*the current script hardcodes to Db1
6. go to client page and enter a couple of todos
7. watch the docker-compose logs for updates upon entering a new todo

### To set authentication to couchdb

1. create admin for couch. This ensures no one can read the db _optional_
2. set user roles for db1. give roles name of `admin_user` for admin and `dev1` for user
   with these settings, the user is required to pass in headers to allow couchdb to authenticate.
